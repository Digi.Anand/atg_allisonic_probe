import socket
import serial
import time


class ATG_allisonicprobe(object):
    def __init__(self, protocol: str, protocolparams: dict):
        self.protocol = protocol
        self.protocolparams = protocolparams
        if self.protocol == "tcp":
            self.ser = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        elif self.protocol == "rs232":
            self.ser = None
        elif self.protocol == "rs485":
            self.ser = serial.Serial(protocolparams["port"], timeout=None)
        else:
            self.ser = None

        self._initialize_connection()

    def _initialize_connection(self):
        if self.protocol == "tcp":
            self.ser.connect((self.protocolparams["ip"], int(self.protocolparams["port"])))

    def terminate_connection(self):
        if self.protocol == "tcp":
            self.ser.close()

    def _send_req(self, data: bytes, response_length: int):
        self.ser.write(data=data)
        resp = self.ser.read(response_length)
        return resp

    def get_version_V(self, probe_id):
        """
        Question    :   V080500{CR}
        Response    :   V080500_4.4.9_80
        Where,
            4.4.9   :   probe firmware version
            80      :   80 Mhz
        """
        data_sent = bytes("V" + str(probe_id) + '\r', encoding="ascii")
        print("sending %s " % data_sent)
        raw_resp = self._send_req(data_sent, 14)
        print(raw_resp)
        raw_resp = raw_resp[:-1]
        resp = raw_resp.decode("ascii").split("_")
        command, probe_id = str(resp[0][:1]), int(resp[0][1:])
        probe_firm_version = float(resp[1])
        probe_freq_Hz = int(resp[1])
        version_received = {
            "probe_id": probe_id,
            "probe_firm_version": probe_firm_version,
            "probe_freq_Hz": probe_freq_Hz
        }
        if len(version_received) == 3:
            print(f"for {probe_id} the version is {probe_firm_version} with {probe_freq_Hz} frequency")
            return version_received

    def get_measure_M(self, probe_id):
        """
        Question      :   M080500{CR}
        For,
        Response OK   :   080500_0_00954.65_00584.11_+206_046{CR}
        Where,
            080500    :   address
            0         :   status (0=ok, 1=measure issue)
            00954.65  :   954.65mm, product level (mm)
            00584.11  :   84.11mm, water level (mm)
            +206      :   20.6, temperature (C°), must divide by 10
            046       :   checksums (CRC-8)

        Response ERROR →080500{LF}{CR}
        Where,
            080500    :   address
        """
        data_sent = bytes("M" + str(probe_id) + '\r', encoding="ascii")
        print("sending %s " % data_sent)
        raw_resp = self._send_req(data_sent, 35)
        print(raw_resp)
        # raw_resp = raw_resp[:-1]
        resp = raw_resp.decode("ascii").split("_")
        probe_id = int(resp[0])
        status = int(resp[1])
        product_level_mm = float(resp[2])
        water_level_mm = float(resp[3])
        temperature = float(resp[4]) * 0.1
        received_chk_sum = int(resp[5][1:3])
        calculated_chk_sum = str(sum([val for val in raw_resp[:-3]]) % 255)
        while len(calculated_chk_sum) < 3:
            calculated_chk_sum = "0" + calculated_chk_sum
        print("received: %s, calc: %s" % (received_chk_sum, calculated_chk_sum))
        data_doc = {
            "probe_id": probe_id,
            "Status": status,
            "product_level_mm": product_height_mm,
            "water_level_mm": water_height_mm,
            "temperature": temperature,
            "chk_sum": received_chk_sum,

        }
        return received_chk_sum == calculated_chk_sum, data_doc

    def get_tank_measurement(self, category: str, probe_id: str):
        data = {}
        if category == "measurement":
            chk_sum_valid, data = self.get_measure_M(probe_id)
            while not chk_sum_valid:
                print("invalid check sum, retrying..")
                chk_sum_valid, data = self.get_measure_M(probe_id)
                time.sleep(0.3)
        return data

    def get_density_d(self, probe_id):
        """
        Question        :   d080500{CR}
        For,
        Response OK     :   d080500_0.8350_001234_00323.11_00123.11_00200.0=105{CR}
        Where,
            080500      :   address
            0.8350      :   0.8350 density product
            001234      :   1234 (INTERNAL USE ONLY)
            00323.11    :   323.11 product float level
            00123.11    :   123.11 density float level
            00200.0     :   200.0 product float level - density float level
            105         :   checksum (CRC-8)
        """
        data_sent = bytes("d" + str(probe_id) + "\r", encoding="ascii")
        print("sending %s " % data_sent)
        raw_resp = self._send_req(data_sent, 51)
        print(raw_resp)
        # raw_resp = raw_resp[:-1]
        resp = raw_resp.decode("ascii").split("_")
        command, probe_id = str(resp[0][:1]), int(resp[0][1:])
        density_product = float(resp[1])
        # internal_1 = int(resp[2])
        product_float_level = float(resp[3])
        density_float_level = float(resp[4])
        level_difference = float((resp[5].split("="))[0])
        received_chk_sum = int((resp[5].split("="))[1][:3])
        calculated_chk_sum = str(sum([val for val in raw_resp[:-3]]) % 255)
        while len(calculated_chk_sum) < 3:
            calculated_chk_sum = "0" + calculated_chk_sum
        print("received: %s, calc: %s" % (received_chk_sum, calculated_chk_sum))
        data_doc = {
            "probe_id": probe_id,
            "density_product": density_product,
            # "internal_1": internal,
            "product_float_level": product_float_level,
            "density_float_level": density_float_level,
            "level_difference": level_difference,
            "chk_sum": received_chk_sum,

        }
        return received_chk_sum == calculated_chk_sum, data_doc

    def get_tank_density(self, category: str, probe_id: str):
        data = {}
        if category == "density":
            chk_sum_valid, data = self.get_density_d(probe_id)
            while not chk_sum_valid:
                print("invalid check sum, retrying..")
                chk_sum_valid, data = self.get_density_d(probe_id)
                time.sleep(0.3)
        return data

    def get_diagnostics_D(self, probe_id):
        """
        Question        :   D080500{CR}
        For,
        Response OK     :   D080500_00_18_0024569_0005972_0053815_+263_0053834_1.0004_0024561_0005970_02_1_0_000_0=236{CR}
        Where,
            D080500     :   command + address
            00          :   status (0=ok, 1=measure issue)
            18          :   INTERNAL USE ONLY
            0024569     :   24569 – PRODUCT float frontward impulse
            0005972     :   5972 – WATER float frontward impulse
            0053815     :   53815 – TOTAL frontward impulses
            +263        :   26.3, temperature (C°), must divide by 10
            0053834     :   53834 –TOTAL backward impulses
            1.0004      :   INTERNAL USE ONLY
            0024561     :   24561 –PRODUCT float backward impulse
            0005970     :   5970 - WATER float backward impulse
            02          :   INTERNAL USE ONLY
            1           :   INTERNAL USE ONLY
            0           :   INTERNAL USE ONLY
            000         :   INTERNAL USE ONLY
            0           :   INTERNAL USE ONLY
            236         :   checksum (CRC-8)
        """
        data_sent = bytes("D" + str(probe_id) + "\r", encoding="ascii")
        print("sending %s " % data_sent)
        raw_resp = self._send_req(data_sent, 90)
        print(raw_resp)
        # raw_resp = raw_resp[:-1]
        resp = raw_resp.decode("ascii").split("_")
        command, probe_id = str(resp[0][:1]), int(resp[0][1:])
        status = int(resp[1])
        # internal_1 = (resp[2])
        product_float_front_impulse = int(resp[3])
        water_float_front_impulse = int(resp[4])
        total_front_impulse = int(resp[5])
        temperature = float(resp[6]) * 0.1
        total_back_impulse = int(resp[7])
        # internal_2 = (resp[8])
        product_float_back_impulse = int(resp[9])
        water_float_back_impulse = int(resp[10])
        # internal_3 = (resp[11])
        # internal_4 = (resp[12])
        # internal_5 = (resp[13])
        # internal_6 = (resp[14])
        # internal_7 = float((resp[15].split("="))[0])
        received_chk_sum = int((resp[15].split("="))[1][:3])
        calculated_chk_sum = str(sum([val for val in raw_resp[:-3]]) % 255)
        while len(calculated_chk_sum) < 3:
            calculated_chk_sum = "0" + calculated_chk_sum
        print("received: %s, calc: %s" % (received_chk_sum, calculated_chk_sum))
        data_doc = {
            "probe_id": probe_id,
            "Status": status,
            # "internal_1": internal_1,
            "product_float_front_impulse": product_float_front_impulse,
            "water_float_front_impulse": water_float_front_impulse,
            "temperature": temperature,
            "total_back_impulse": total_back_impulse,
            # "internal_2": internal_2,
            "product_float_back_impulse": product_float_back_impulse,
            "water_float_back_impulse": water_float_back_impulse,
            # "internal_3": internal_3,
            # "internal_4": internal_4,
            # "internal_5": internal_5,
            # "internal_6": internal_6,
            # "internal_7": internal_7,
            "chk_sum": received_chk_sum

        }
        return received_chk_sum == calculated_chk_sum, data_doc

    def get_tank_diagnosis(self, category: str, probe_id: str):
        data = {}
        if category == "diagnosis":
            chk_sum_valid, data = self.get_diagnostics_D(probe_id)
            while not chk_sum_valid:
                print("invalid check sum, retrying..")
                chk_sum_valid, data = self.get_diagnostics_D(probe_id)
                time.sleep(0.3)
        return data


if __name__ == "__main__":
    allisonic_comm = ATG_allisonicprobe("rs485", {"port": "/dev/ttyAMA0"})
    probe_id_list = ["080500", "080500", "080500"]
    category_list = ["measurement", "density", "diagnosis"]
    for probe_id in probe_id_list:
        for category in category_list:
            if category == "measurement":
                r = allisonic_comm.get_tank_measurement("measurement", probe_id)
                print(r)
                break
            elif category == "density":
                s = allisonic_comm.get_tank_density("density", probe_id)
                print(s)
                break
            elif category == "diagnosis":
                t = allisonic_comm.get_tank_diagnosis("density", probe_id)
                print(t)
                break
            else:
                print("Enter Correct Category")